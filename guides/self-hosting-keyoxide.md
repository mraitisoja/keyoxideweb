# Self-hosting Keyoxide

Though it's not a fully supported use case yet, anyone can take the [source code](https://codeberg.org/keyoxide/web) and put it on their own server. The idea is that [Keyoxide.org](https://keyoxide.org) is not special in itself. After all, all the heavy lifting is done by the browser. So the role of any individual Keyoxide server is to get the tool in the hands of the end user.

The few supporting roles the server has can easily be performed by any other (PHP) server.

So if you like the project but perhaps are mistrusting of servers of others, especially when it comes to keypairs, here's the [source code](https://codeberg.org/keyoxide/web) and put it on your own server. Thanks for using the project!
